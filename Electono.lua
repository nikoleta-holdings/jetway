--whitelist
wait(5)
if script.Parent.AU.Value == '59703373367639792F423F4528482B4D6251655468576D5A7134743777217A25432646294A404E635266556A586E3272357538782F413F4428472D4B6150645367566B5970337336763979244226452948404D6251655468576D5A7134743777217A25432A462D4A614E645266556A586E3272357538782F413F4428472B4B6250655368566B5970337336763979244226452948404D635166546A576E5A7134743777217A25432A462D4A614E645267556B58703273357638782F413F4428472B4B6250655368566D597133743677397A244226452948404D635166546A576E5A7234753778214125442A462D4A614E645267556B58703273357638792F423F' then
	script.Parent.AU.Value = '0'
	warn('Nikoleta | Successfully activated.')
else
	script:Destroy()
end
--- Identification ---

-- Locations --

local Pressed = script.Local.Value:WaitForChild("Pressed")
local RS = game:GetService("RunService")
local Cab = script.Parent.Parent:WaitForChild("Cab")
local Terminal = script.Parent.Parent:WaitForChild("Terminal_Attachment")
local Tunnels = script.Parent.Parent:WaitForChild("Tunnels")
local Stairs = script.Parent.Parent:WaitForChild("Stairs")
local Elevation = script.Parent.Parent:WaitForChild("Elevation")
local CabD = script.Parent.Parent.Parent.Cab:WaitForChild("Degrees")
local TerminalD = script.Parent.Parent.Parent.Terminal_Attachment:WaitForChild("Degrees")
local Lights = script.Parent.Parent.Parent:WaitForChild("Lights")
local Degree_Values = script.Parent.Parent:WaitForChild("Degrees")
local MainD = script.Parent.Parent.Parent:WaitForChild("Terminal_Attachment"):WaitForChild("Degrees")
local Sounds = script.Parent.Parent.Parent.Terminal_Attachment.Main
local HD = false
local Setup = require(script.Parent.Electono_Setup)
local Seat = script.Parent.Parent.Parent.Jetway_Controller
local Speed = 0

local Jetway = {}

-- Number Values --

local CabR = 0
local Steering = 0
local Tunnel = 0
local MainR = 0
local ElevationV = 0
local Torque = 0
local CAttachment = 0
local TorqueSpeed = 0

-- Bool Values --

local TorqueKey = false
local ElevationKey = false
local Cab_Doors = false
local Beacons = false
local Interior_Lights = false
local Headlights = false
local Frozen = false
local KeyPressed = "Up"

-- Keys --

-- Freeze, Reset

local P_KEY = Enum.KeyCode.P
local Y_KEY = Enum.KeyCode.Y

-- Torque

local W_KEY = Enum.KeyCode.W
local S_KEY = Enum.KeyCode.S
local UP_KEY = Enum.KeyCode.Up
local DWN_KEY = Enum.KeyCode.Down

-- Steering

local Z_KEY = Enum.KeyCode.Z
local X_KEY = Enum.KeyCode.X
local C_KEY = Enum.KeyCode.C

-- Cab

local A_KEY = Enum.KeyCode.A
local D_KEY = Enum.KeyCode.D
local R_KEY = Enum.KeyCode.R
local F_KEY = Enum.KeyCode.F

-- Elevation

local E_KEY = Enum.KeyCode.E
local Q_KEY = Enum.KeyCode.Q

-- Lights

local L_KEY = Enum.KeyCode.L
local H_KEY = Enum.KeyCode.H
local B_KEY = Enum.KeyCode.B

-- Doors

local G_KEY = Enum.KeyCode.G

--- Initialize ---

function Init()
	
	for i,v in pairs(Degree_Values.Cab.Part:GetChildren()) do
		for i,d in pairs(CabD.Part:GetChildren()) do
			if v.Name == d.Name then
				v.Value = d
			end
		end
	end
	for i,v in pairs(Degree_Values.Cab.Glass:GetChildren()) do
		for i,d in pairs(CabD.Glass:GetChildren()) do
			if v.Name == d.Name then
				v.Value = d
			end
		end
	end
	for i,v in pairs(Degree_Values.Terminal.Part:GetChildren()) do
		for i,d in pairs(TerminalD.Part:GetChildren()) do
			if v.Name == d.Name then
				v.Value = d
			end
		end
	end
	for i,v in pairs(Degree_Values.Terminal.Glass:GetChildren()) do
		for i,d in pairs(TerminalD.Glass:GetChildren()) do
			if v.Name == d.Name then
				v.Value = d
			end
		end
	end
	
	for _,v in pairs(script.Parent.Parent.Parent.Cab:GetChildren()) do
		if v:IsA("BasePart") or v:IsA("MeshPart") then
			table.insert(Jetway, v)
		end
	end
	for _,v in pairs(script.Parent.Parent.Parent.Tunnel_Two:GetChildren()) do
		if v:IsA("BasePart") or v:IsA("MeshPart") then
			table.insert(Jetway, v)
		end
	end
	for _,v in pairs(script.Parent.Parent.Parent.Tunnel_One:GetChildren()) do
		if v:IsA("BasePart") or v:IsA("MeshPart") then
			table.insert(Jetway, v)
		end
	end
	for _,v in pairs(script.Parent.Parent.Parent.Stairs:GetChildren()) do
		if v:IsA("BasePart") or v:IsA("MeshPart") then
			table.insert(Jetway, v)
		end
	end
	for _,v in pairs(script.Parent.Parent.Parent.Gear:GetChildren()) do
		if v:IsA("BasePart") or v:IsA("MeshPart") then
			table.insert(Jetway, v)
		end
	end
	for _,v in pairs(script.Parent.Parent.Parent.Lights:GetChildren()) do
		if v:IsA("BasePart") or v:IsA("MeshPart") then
			table.insert(Jetway, v)
		end
	end
	for _,v in pairs(script.Parent.Parent.Parent.Terminal_Attachment:GetChildren()) do
		if v.Name == "Connection" or v.Name == "Exterior" or v.Name == "Vent" then
			if v:IsA("BasePart") or v:IsA("MeshPart") then
				table.insert(Jetway, v)
			end
		end
	end
	
	table.insert(Jetway, Seat)
	
end

--- Handle Local Control Presses ---

Pressed.OnServerEvent:Connect(function(Player, Key)
	
	if Key ~= "Up" then
		
		KeyPressed = Key
		
		if Key == G_KEY then -- Doors
		
			if Cab_Doors == false then
				Cab.Cab_Door_Right.TargetAngle = -90
				Cab.Cab_Door_Left.TargetAngle = -90
				Cab_Doors = true
			elseif Cab_Doors == true then
				Cab.Cab_Door_Right.TargetAngle = 0
				Cab.Cab_Door_Left.TargetAngle = 0
				Cab_Doors = false
			end	
		
		elseif Key == B_KEY then -- Lights Beacon
		
			if Beacons == false then
				Cab.Beacons.AngularVelocity = 2.5
				Lights.Caution_Lights.Material = "Neon"
				Lights.Caution_Lights.FL.Enabled = true
				Lights.Caution_Lights.FR.Enabled = true
				Lights.Caution_Lights.Lamp.Light.Enabled = true
				Beacons = true
			elseif Beacons == true then
				Cab.Beacons.AngularVelocity = 0
				Lights.Caution_Lights.Material = "SmoothPlastic"
				Lights.Caution_Lights.FL.Enabled = false
				Lights.Caution_Lights.FR.Enabled = false
				Lights.Caution_Lights.Lamp.Light.Enabled = false
				Beacons = false
			end
		
		elseif Key == H_KEY then -- Lights Headlights
			
			if Headlights == false then
				Lights.Headlights.Light.Enabled = true
				for i,v in pairs(Lights.Headlights.Beams:GetChildren()) do
					v.Enabled = true
				end	
				Headlights = true
			elseif Headlights == true then
				Lights.Headlights.Light.Enabled = false
				for i,v in pairs(Lights.Headlights.Beams:GetChildren()) do
					v.Enabled = false
				end	
				Headlights = false
			end	
	
		elseif Key == L_KEY then -- Lights Interior
			
			if Interior_Lights == false then
				for i,v in pairs(Lights.Interior_Lights:GetChildren()) do
					v.Material = "Neon"
					v.Light.Enabled = true
				end	
				Interior_Lights = true
			elseif Interior_Lights == true then
				for i,v in pairs(Lights.Interior_Lights:GetChildren()) do
					v.Material = "SmoothPlastic"
					v.Light.Enabled = false
				end	
				Interior_Lights = false
			end
			
		elseif Key == P_KEY then -- Freeze
			
			--if Speed == 0 then
	
				if Frozen == false then
			
					for i,v in pairs(Jetway) do
						v.Anchored = true
					end
					Frozen = true
		
				elseif Frozen == true then
		
					for i,v in pairs(Jetway) do
						v.Anchored = false
					end
					Frozen = false
		
				end
		
			--end
			
		end	
		
	elseif Key == "Up" then
		
		KeyPressed = "Up"
		
	end
	
end)

--- Update System ---

-- Cab Degrees --

Cab.Cab.Changed:Connect(function()
	
	local LA = CabR - 50
	local UA = CabR + 50
	
	-- Part --
	
	for i,v in pairs(Degree_Values.Cab.Part:GetChildren()) do
		if tonumber(v.Value.Name) <= UA and tonumber(v.Value.Name) >= LA then
			v.Value.Transparency = 1
			v.Value.CanCollide = false
		else
			v.Value.Transparency = 0
			v.Value.CanCollide = true
		end	
	end
	
	-- Glass --
	
	for i,v in pairs(Degree_Values.Cab.Glass:GetChildren()) do
		if tonumber(v.Value.Name) <= UA and tonumber(v.Value.Name) >= LA then
			v.Value.Transparency = 1
		else
			v.Value.Transparency = 0.4
		end
	end
	
end)

-- Terminal Degrees --

Terminal.Rotation_Slide.Changed:Connect(function()
	
	local LA = MainR - 65
	local UA = MainR + 65
	
	-- Part --
	
	for i,v in pairs(Degree_Values.Terminal.Part:GetChildren()) do
		if tonumber(v.Value.Name) <= UA and tonumber(v.Value.Name) >= LA then
			v.Value.Transparency = 1
			v.Value.CanCollide = false
		else
			v.Value.Transparency = 0
			v.Value.CanCollide = true
		end	
	end
	
	-- Glass --
	
	for i,v in pairs(Degree_Values.Terminal.Glass:GetChildren()) do
		if tonumber(v.Value.Name) <= UA and tonumber(v.Value.Name) >= LA then
			v.Value.Transparency = 1
		else
			v.Value.Transparency = 0.4
		end	
	end
	
end)

-- Update Keys --

RS.Heartbeat:Connect(function(Key)
	
	--local SpeedRaw = math.floor(Seat.Velocity.Magnitude)
	--Speed = SpeedRaw
	
	--- Keys ---
	
	Key = KeyPressed
	
	-- Check for TorqueKey bool --
	
	if Key ~= W_KEY and Key ~= S_KEY then
		Torque = 0
		Elevation.WHL_L.AngularVelocity = 0
		Elevation.WHL_R.AngularVelocity = 0
		TorqueKey = false
	elseif Key ~= E_KEY and Key ~= Q_KEY then
		ElevationKey = false
	end
	

	--- Check Values ---
	
	-- Lock Slide --
	
	if ElevationKey == true then
		Elevation.Elevation_Slide.ActuatorType = "None"
		Stairs.Keep_Stable.ActuatorType = "None"
	elseif ElevationKey == false then
		Elevation.Elevation_Slide.ActuatorType = "Servo"
		Stairs.Keep_Stable.ActuatorType = "Servo"
		Stairs.Keep_Stable.TargetAngle = Stairs.Keep_Stable.CurrentAngle
		Elevation.Elevation_Slide.TargetPosition = Elevation.Elevation_Slide.CurrentPosition
	end
	
	if TorqueKey == true and TorqueSpeed ~= 0 then
		Elevation.Elevation_Slide.ActuatorType = "None"
		Tunnels.Tunnel_Two.ActuatorType = "None"
		Sounds.Hydraulics.Volume = 0.5
		Sounds.Bell.Playing = true
	elseif TorqueKey == false then
		Tunnels.Tunnel_Two.ActuatorType = "Servo"
		Tunnels.Tunnel_Two.TargetPosition = Tunnels.Tunnel_Two.CurrentPosition
		if not HD then
			Sounds.Hydraulics.Volume = 0
			Sounds.Bell.Playing = false
		end
	end
	
	Elevation.Keep_Stable.TargetAngle = math.clamp(-ElevationV, -3, 4.5)
	Cab.Keep_Stable.TargetAngle = -ElevationV
	Terminal.Rotation_Slide.ActuatorType = "Servo"
	Terminal.Rotation_Slide.ActuatorType = "None"
	MainR = Terminal.Rotation_Slide.CurrentAngle
	Tunnel = Tunnels.Tunnel_Two.CurrentPosition
	
	-- Gui Based --
	
	script.TS.Value = TorqueSpeed
	
	-- Main Movement --
	if Frozen == false then
	if Key == E_KEY then -- Elevation
		
		Terminal.Elevation.TargetAngle = math.clamp(ElevationV + 0.1, -5.5, 3)
		ElevationV = math.clamp(ElevationV + 0.05, -4.5, 2)
		ElevationKey = true
		
	elseif Key == Q_KEY then -- Elevation
		
		Terminal.Elevation.TargetAngle = math.clamp(ElevationV - 0.1, -5.5, 3)
		ElevationV = math.clamp(ElevationV - 0.05, -4.5, 2)
		ElevationKey = true
	
	elseif Key == R_KEY then -- Attachment
		
		Cab.Attachment.TargetAngle = math.clamp(CAttachment + 0.1, 0, 10)
		CAttachment = math.clamp(CAttachment + 0.05, 0, 10)
		
	elseif Key == F_KEY then -- Attachment
		
		Cab.Attachment.TargetAngle = math.clamp(CAttachment + 0.1, 0, 10)
		CAttachment = math.clamp(CAttachment - 0.05, 0, 10)
	
	elseif Key == Z_KEY then -- Steer Left
		
		Elevation.Steering.TargetAngle = math.clamp(Steering + 1, -90, 90)
		Steering = math.clamp(Steering + 1, -90, 90)
		
	elseif Key == X_KEY then -- Steer Reset
			
		Elevation.Steering.TargetAngle = 0
		Steering = 0
		
	elseif Key == C_KEY then -- Steer Right
			
		Elevation.Steering.TargetAngle = math.clamp(Steering - 1, -90, 90)
		Steering = math.clamp(Steering - 1, -90, 90)
		
	elseif Key == UP_KEY then -- Torque Speed Increase
		
		TorqueSpeed = math.clamp(TorqueSpeed + 0.1, 0, 4)
		
	elseif Key == DWN_KEY then -- Torque Speed Decrease
		
		TorqueSpeed = math.clamp(TorqueSpeed - 0.1, 0, 4)
		
	elseif Key == W_KEY then -- Torque Accelerate
		
		if TorqueSpeed ~= 0 then
			Sounds.HydraulicsS.Playing = true
		end	
		Elevation.WHL_R.AngularVelocity = math.clamp(Torque + 0.2 * TorqueSpeed * 10, -4, 4)
		Elevation.WHL_L.AngularVelocity = math.clamp(Torque + 0.2 * TorqueSpeed * 10, -4, 4)
		Torque = math.clamp(Torque + 0.4 * TorqueSpeed * 10, -2, 2)
		TorqueKey = true
		
	elseif Key == S_KEY then -- Torque Decelerate
		
		if TorqueSpeed ~= 0 then
			Sounds.HydraulicsS.Playing = true
		end	
		Elevation.WHL_R.AngularVelocity = math.clamp(Torque - 0.2 * TorqueSpeed * 10, -4, 4)
		Elevation.WHL_L.AngularVelocity = math.clamp(Torque - 0.2 * TorqueSpeed * 10, -4, 4)
		Torque = math.clamp(Torque - 0.4 * TorqueSpeed * 10, -4, 4)
		TorqueKey = true
		
	elseif Key == A_KEY then -- Cab Left
	
		Cab.Cab.TargetAngle = math.clamp(CabR + 0.1, -15, 90)
		CabR =  math.clamp(CabR + 0.1, -15, 90)
	
	elseif Key == D_KEY then -- Cab Right
		
		Cab.Cab.TargetAngle = math.clamp(CabR - 0.1, -15, 90)
		CabR =  math.clamp(CabR - 0.1, -15, 90)
		
	--[[elseif Key == Y_KEY then -- Reset
			
		if Frozen == false then
				
			if Terminal.Rotation_Slide.CurrentAngle ~= 0 then
				
				TorqueKey = true
				ElevationKey = true
				CabR = 0
				Cab.Cab.TargetAngle = 0
				ElevationV = 0
				Terminal.Elevation.TargetAngle = 0
				Cab.Attachment.TargetAngle = 0
				CAttachment = 0
				Elevation.Steering.TargetAngle = 90
				Steering = 90
				print("Steering Phase")
				Elevation.Steering.ActuatorType = "None"
				Elevation.Steering.ActuatorType = "Servo"
				repeat wait() until Elevation.Steering.CurrentAngle >= 90
				print("Rotation Phase")
				Elevation.WHL_L.AngularVelocity = 4
				Elevation.WHL_R.AngularVelocity = 4
				Torque = 4
				repeat wait() until Terminal.Rotation_Slide.CurrentAngle <= 0
				Elevation.WHL_L.AngularVelocity = 0
				Elevation.WHL_R.AngularVelocity = 0
				Torque = 0
				Steering = 0
				Elevation.Steering.TargetAngle = 0
				Elevation.Steering.ActuatorType = "None"
				Elevation.Steering.ActuatorType = "Servo"
				repeat wait() until Elevation.Steering.CurrentAngle <= 0
				if Tunnels.Tunnel_Two.CurrentPosition ~= 0 then
					print("Tunnel Phase")
					Elevation.WHL_L.AngularVelocity = 4
					Elevation.WHL_R.AngularVelocity = 4
					Torque = 4
					repeat wait() until Tunnels.Tunnel_Two.CurrentPosition <= 0
					TorqueKey = false
					Elevation.WHL_L.AngularVelocity = 0
					Elevation.WHL_R.AngularVelocity = 0
					Torque = 0
					ElevationKey = false
					print("Jetway Reset Successful")
				end
					
			end
			
			TorqueKey = true
			ElevationKey = true
			Terminal.Rotation_Slide.ActuatorType = "Servo"
			Tunnels.Tunnel_Two.ActuatorType = "Servo"
			Cab.Cab.TargetAngle = 0
			Terminal.Elevation.TargetAngle = 0
			Cab.Attachment.TargetAngle = 0
			Elevation.Steering.TargetAngle = 0
			Terminal.Rotation_Slide.TargetAngle = 0
			Steering = 0
			MainR = 0
			Tunnel = 0
			CAttachment = 0
			ElevationV = 0
			CabR = 0
			wait(10)
			
		end	--]]
		
	end
	end
	
end)

--- Initialize ---

Init()