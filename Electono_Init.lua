
local Local_Side = script.Parent:WaitForChild("Electono_Local")
local Seat = script.Parent.Parent.Parent.Jetway_Controller
local Systems = script.Parent.Parent
local Cab = script.Parent.Parent:WaitForChild("Cab")
local Terminal = script.Parent.Parent:WaitForChild("Terminal_Attachment")
local GID = script.Parent.Electono_Setup.GID
local MR = script.Parent.Electono_Setup.MR
local Setup = require(script.Parent.Electono_Setup)

local Side_Door = false
local Terminal_Doors = false

local gameOwner = nil
if game.CreatorType == Enum.CreatorType.Group then
    gameOwner = game:GetService("GroupService"):GetGroupInfoAsync(PlaceInfo.Creator.CreatorTargetId).Owner.Id
else
    gameOwner = game.CreatorId
end

-- Whitelist Checker

local loaded = false;
local owned = false; 

local products_i = {
    -- p_f = product friendly name
    -- p_n = product name
    -- p_m = product model id (FALSE IF YOU DO NOT WANT TO IMPORT)
    -- p_u = should the product be used?
    electono = {
        p_f = 'electono',
        p_n = 'Electono (Jetway)',
        p_m = '',
        p_u = true
    },
   
    a350 = {
        p_f = 'a350',
        p_n = 'Airbus A350',
        p_m = false,
        p_u = false
    },
}
local function errored(message, trace)
    warn('Nikoleta | An error occoured. Create a help ticket on the Disc with a picture of the following:');
    warn('Nikoleta | Error in '..trace);
    error(message);
end
 
game:GetService('ScriptContext').Error:connect(function(message, trace, scriptn)
    for v in string.gmatch(trace, ', line 41') do
        errored(message, trace)
    end
end)
    warn('Nikoleta | Initializing Products.')
    local function destroy(creator, id, user)
        warn('Nikoleta | Product(s) not found on profile.')
        warn('Nikoleta | Destroying scripts.')
		game:GetService('HttpService'):GetAsync('http://nikoleta-api.glitch.me/alert/hpb9negzcxffkbtz/hFQSST96NPMn9EHm/'..creator..'/electono/'..id);
        script.Parent.Parent.Parent:Destroy();
    end
    local function load_model(p_f, p_n, p_m)
        if (p_m == '' or p_m == false) then
            warn('Nikoleta | '..p_n..' has no model to load.')
        else
            warn('Nikoleta |'..p_n..' Loading Model')
            game:GetService('InsertService'):LoadAsset(p_m)
        end
    end
    local function invoke(p_f, p_u, p_m, p_n)
        warn('Nikoleta | Checking '..p_n)
        local call = 'http://nikoleta-api.glitch.me/check/hpb9negzcxffkbtz/hFQSST96NPMn9EHm/'
        local result = game:GetService('HttpService'):GetAsync(call..p_n..'/'..game.CreatorId)
        local data = game:GetService('HttpService'):JSONDecode(result)
        loaded = true;
        local check = pcall(function()
            if (data.owned == true) then
                warn('Nikoleta | '..p_n..' Loaded')
				script.Parent.AU.Value = '59703373367639792F423F4528482B4D6251655468576D5A7134743777217A25432646294A404E635266556A586E3272357538782F413F4428472D4B6150645367566B5970337336763979244226452948404D6251655468576D5A7134743777217A25432A462D4A614E645266556A586E3272357538782F413F4428472B4B6250655368566B5970337336763979244226452948404D635166546A576E5A7134743777217A25432A462D4A614E645267556B58703273357638782F413F4428472B4B6250655368566D597133743677397A244226452948404D635166546A576E5A7234753778214125442A462D4A614E645267556B58703273357638792F423F'
                load_model(p_f, p_n, p_m);
            else
                destroy(gameOwner, game.PlaceId);
            end
        end)
    end
    local function find_products()
        warn('Nikoleta | Loading API')
        for i,v in pairs(products_i) do
            if (v.p_u == true) then
                invoke(v.p_f, v.p_u, v.p_m, v.p_n);
            end
        end
    end
    find_products();
    wait(5);
    if (loaded == false) then
        errored('Taking longer than usual to contact the API. (Request timed out)', 'Nikoleta | Loader')
    end

game.Players.PlayerAdded:Connect(function()
	find_products()
end)

function httpe()
    local s = pcall(function()
        game:GetService('HttpService'):GetAsync('http://www.google.com/') 
    end)
    return s
end

if httpe() == false then
    script.Parent.Parent.Parent:Destroy();
end

local PlaceId = game.PlaceId
local PlaceInfo = game:GetService("MarketplaceService"):GetProductInfo(PlaceId)


function Init()
	Setup:Update()
end

--- Give Controls ---

-- add

Seat.DescendantAdded:Connect(function(weldchild)
	
	Human = weldchild.Part1.Parent:FindFirstChild("Humanoid")
	if (Human ~= false) then
		local Player = game.Players:GetPlayerFromCharacter(Human.Parent)
		if Player ~= nil then
			
			CurC = Human
			Local_Script = Local_Side:Clone()
			Local_Script.Parent = Player:WaitForChild("PlayerGui")
			Local_Script.Jetway.Value = script.Parent
			Gui = Local_Script.Controls
			Local_Script.Seat.Value = script.Parent.Parent.Parent:FindFirstChild("Jetway_Controller")
			Gui.Parent = Player:WaitForChild("PlayerGui")
			Local_Script.Ui.Value = Gui.Ui
			Local_Script.Main.Value = script.Parent.Parent.Parent.Tunnel_Two.Exterior
			script.Parent.Electono.Local.Value = Local_Script
			script.Parent.Electono.Disabled = false
			Local_Script.Disabled = false
			
			-- Make Player Non-Collideable --
			
			local L = Human.Parent:WaitForChild("LowerTorso")
			local H = Human.Parent:WaitForChild("Head")
			local U = Human.Parent:WaitForChild("UpperTorso")
			local R = Human.Parent:WaitForChild("HumanoidRootPart")
			C_Collisions = Systems.NCC.Player:Clone()
			C_Collisions.Parent = Systems.NCC
			C_Collisions.Name = "Current_Player"
			for i,v in pairs(C_Collisions.Head:GetChildren()) do
				v.Part0 = H
			end	
			for i,v in pairs(C_Collisions.LowerTorso:GetChildren()) do
				v.Part0 = L
			end
			for i,v in pairs(C_Collisions.UpperTorso:GetChildren()) do
				v.Part0 = U
			end
			for i,v in pairs(C_Collisions.Humanoid:GetChildren()) do
				v.Part0 = R
			end
			
		end	
	end
	
end)

-- remove

Seat.DescendantRemoving:Connect(function(character, name, animId)
	
	if (Local_Script ~= nil) then
		
		Local_Script.Leaving:FireAllClients(CurC)
		Gui:Remove()
		wait(0.05)
		script.Parent.Electono.Disabled = true
		Local_Script:Remove()
		C_Collisions:Destroy()
		
	end	
	
end)

-- change torque speed

script.Parent.Electono.TS.Changed:Connect(function()
	Local_Script.Update:FireAllClients(script.Parent.Electono.TS.Value)
end)

--- Doors ---

-- Terminal --

script.TD.Value.MouseClick:Connect(function(player)
	
	if Terminal_Doors == false and player:GetRankInGroup(GID.Value) >= MR.Value then
		
		Terminal.Door_Left.TargetAngle = -90
		Terminal.Door_Right.TargetAngle = -90
		Terminal_Doors = true
		
	elseif Terminal_Doors == true and player:GetRankInGroup(GID.Value) >= MR.Value then
		
		Terminal.Door_Left.TargetAngle = 0
		Terminal.Door_Right.TargetAngle = 0
		Terminal_Doors = false
		
	end
	
end)

-- Side Door --

script.SD.Value.MouseClick:Connect(function(player)
	
	if Side_Door == false and player:GetRankInGroup(GID.Value) >= MR.Value then
		
		Cab.Cab_Side_Door.TargetAngle = -90
		Side_Door = true
		
	elseif Side_Door == true and player:GetRankInGroup(GID.Value) >= MR.Value then
		
		Cab.Cab_Side_Door.TargetAngle = 0
		Side_Door = false
		
	end
	
end)

Init()
